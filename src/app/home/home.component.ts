import { Component, OnInit } from "@angular/core";
import { AnimationCurve  } from "tns-core-modules/ui/enums";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures";
import { View } from "tns-core-modules/ui/core/view";
import { Page } from "tns-core-modules/ui/page";
import { TNSPlayer } from 'nativescript-audio-player';

@Component({
    selector: "Home",
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']

})
export class HomeComponent implements OnInit {
    private _player: TNSPlayer;

    constructor(private page: Page) { }

    ngOnInit(): void {
    }

    playAudio(id): void{
        let audio = id;
        console.log(" ____<<<<<<<<<<>>>>>>>>____" + audio);
        this._player = new TNSPlayer();
        this._player.debug = true; // set true to enable TNSPlayer console logs for debugging.
        this._player
          .initFromFile({
            audioFile: '~/app/media/home/sound/'+ audio +'.mp3', // ~ = app directory
            loop: false
          });
    }

    onTouch(args: TouchGestureEventData, id, sound) {
        let action = args.action
        let actualElement = this.page.getViewById<View>(id);

        if(action === 'down'){
            this.playAudio(sound);
            if (this._player.isAudioPlaying()) {
                //this._player.pause();
                console.log(" ____<<<<<<<<<<   PAUSE   >>>>>>>>____");
            } else {
                this._player.play();
                console.log(" ____<<<<<<<<<<   PLAY   >>>>>>>>____");
            }
            actualElement.animate({
              scale: { x: 1.2, y: 1.2 },
              curve: AnimationCurve.easeIn,
              duration: 50  
            });
        }else if(action === 'up' || action === 'cancel'){
            actualElement.animate({
              scale: { x: 1, y: 1 },
              curve: AnimationCurve.easeIn,
              duration: 50
            });      
        }else{
            console.log("Action now: " + action);
        }
    }

}
