import { Component, OnInit } from '@angular/core';
import { AnimationCurve  } from "tns-core-modules/ui/enums";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures";
import { View } from "tns-core-modules/ui/core/view";
import { Page } from "tns-core-modules/ui/page";
import { TNSPlayer } from 'nativescript-audio-player';

@Component({
  selector: 'ns-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {
  private _player: TNSPlayer;

  constructor(private page: Page) { }

  ngOnInit(): void {
    console.log("Cargado desde school")
  }

  playAudio(id): void{
    let audio = id;
    this._player = new TNSPlayer();

    if (!this._player.isAudioPlaying()) {

      this._player.debug = true; // set true to enable TNSPlayer console logs for debugging.
      this._player
        .initFromFile({
          audioFile: '~/app/media/school/sound/'+ audio +'.mp3', // ~ = app directory
          loop: false,
          completeCallback: this._trackComplete.bind(this),
          errorCallback: this._trackError.bind(this)
        });
    }else{
      this._player.pause();
    }
  }

  onTouch(args: TouchGestureEventData, id, sound) {
    let action = args.action
    let actualElement = this.page.getViewById<View>(id);

    if(action === 'down'){
        this.playAudio(sound);
        this._player.play();
        actualElement.animate({
          scale: { x: 1.2, y: 1.2 },
          curve: AnimationCurve.easeIn,
          duration: 50  
        });
        this._player.dispose();

    }else if(action === 'up' || action === 'cancel'){
        actualElement.animate({
          scale: { x: 1, y: 1 },
          curve: AnimationCurve.easeIn,
          duration: 50
        });      
    }else{
        console.log("Action now: " + action);
    }
  }

  private _trackComplete(args: any) {
    console.log('reference back to player:', args.player);
    // iOS only: flag indicating if completed succesfully
    console.log('whether song play completed successfully:', args.flag);
  }

    private _trackError(args: any) {
      console.log('reference back to player:', args.player);
      console.log('the error:', args.error);
      // Android only: extra detail on error
      console.log('extra info on the error:', args.extra);
      console.log(args);
      
    }
  animation(x , y) : void{

  }


}
