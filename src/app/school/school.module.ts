import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { SchoolComponent } from './school.component';
import { SchoolRoutingModule } from './school-routing.module';

@NgModule({
  imports: [
    SchoolRoutingModule,
    NativeScriptCommonModule
  ],
  declarations: [SchoolComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SchoolModule { }
