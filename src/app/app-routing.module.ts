import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NSEmptyOutletComponent } from "nativescript-angular";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    {
        path: "",
        redirectTo: "/(homeTab:home/default//schoolTab:school/default//fruitsTab:fruit/default)",
        pathMatch: "full"
    },
    {
        path: "home",
        component: NSEmptyOutletComponent,
        loadChildren: () => import("~/app/home/home.module").then((m) => m.HomeModule),
        outlet: "homeTab"
    },
    {
        path: "school",
        component: NSEmptyOutletComponent,
        loadChildren: () => import("~/app/school/school.module").then((m) => m.SchoolModule),
        outlet: "schoolTab"
    },
    {
        path: "fruit",
        component: NSEmptyOutletComponent,
        loadChildren: () => import("~/app/fruits/fruits.module").then((m) => m.FruitsModule),
        outlet: "fruitsTab"
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
