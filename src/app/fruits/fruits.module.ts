import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { FruitsRoutingModule } from './fruits-routing.module';
import { FruitsComponent } from './fruits.component';


@NgModule({
  imports: [
    NativeScriptCommonModule,
    FruitsRoutingModule
  ],
  declarations: [FruitsComponent],

  schemas: [NO_ERRORS_SCHEMA]
})
export class FruitsModule { }
